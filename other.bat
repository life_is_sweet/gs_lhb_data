@echo off
:: 获取当前计算机名称
set "DEVICE_NAME=%COMPUTERNAME%"
echo 当前设备名称: %DEVICE_NAME%

for /f "tokens=1-3 delims=-/ " %%1 in ("%date%") do set ddd=%%1%%2%%3
for /f "tokens=1-4 delims=.: " %%1 in ("%time%") do set tttt=%%1%%2%%3%%4
Set DT=%ddd%%tttt%

set PROPATH=%cd%
cd "%PROPATH%"
::回到上一级目录
cd ..
::更新mark文件
echo --更新涨停原因标签【有备份】
md mark_bak
echo ---清理7天前备份的标签数据!!
Forfiles /p "%cd%\mark_bak" /s /d -7 /c "cmd /c del /q /f @path"
echo ---备份旧标签数据!!备份位置：

:: 判断设备名称是否不等于特定值并执行相应操作
if "%DEVICE_NAME%" neq "萌柚" (
    echo "%cd%\mark_bak\mark.dat_bak_%DT%"
    move mark.dat mark_bak\mark.dat_bak_%DT%
    echo ---更新涨停原因标签
    copy "%PROPATH%\mark.dat" "%cd%"
)else (
    echo 设备名称为 %COMPUTERNAME%，跳过特定操作...
) 

if "%DEVICE_NAME%" neq "LAPTOP-IJ0LCNEN" (
    echo "%cd%\mark_bak\mark.dat_bak_%DT%"
    move mark.dat mark_bak\mark.dat_bak_%DT%
    echo ---更新涨停原因标签
    copy "%PROPATH%\mark.dat" "%cd%"
)else (
    echo 设备名称为 %COMPUTERNAME%，跳过特定操作...
) 

::更新价投股池
::echo --更新价投股池【不备份，直接覆盖】
::del blocknew\JTGC.blk
::type "%PROPATH%\JTGC.blk" > blocknew\JTGC.blk
::echo --更新价投原因【不备份，直接覆盖】
::type "%PROPATH%\extern_user_jt.txt" >> "%PROPATH%\extern_user.txt"
echo --更新个股概念【不备份，直接覆盖】
type "%PROPATH%\同花顺简数据.txt" >> "%PROPATH%\extern_user.txt"
echo --更新投资主题【不备份，直接覆盖】
type "%PROPATH%\通达信主题投资.txt" >> "%PROPATH%\extern_user.txt"
echo --更新副图涨停原因【不备份，直接覆盖】
type "%PROPATH%\extern_user_tmp.txt" >> "%PROPATH%\extern_user.txt"
echo --创建自选股文件链接 
del /Q /F %cd%\signals\gp\自选股.txt
mklink /H %cd%\signals\gp\自选股.txt %cd%\blocknew\ZXG.blk
echo --创建上影线套利文件链接 
del /Q /F %cd%\signals\gp\上影线套利.txt
mklink /H %cd%\signals\gp\上影线套利.txt %cd%\blocknew\CSYTL.blk
del /Q /F %cd%\signals\gp\昨日涨停.txt
mklink /H %cd%\signals\gp\昨日涨停.txt %cd%\blocknew\JRZT.blk

pause