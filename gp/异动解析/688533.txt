﻿[{20231009}]{"code":"688533","name":"上声电子","date":"2023-10-09","bkmc":"新能源汽车（华为）","reason":"问界新M7自2023年9月12日发布至10月6日，首销累计已经超过5万台大定。","lb":"","expound":"供货赛力斯+车载扬声器+整车音响+三季报预增<br>1、据机构测算，公司2023年来自赛力斯的营收占比约4.9%。<br>2、公司车载扬声器在全球乘用车及轻型商用车市场的占有率为12.92%，国内市场占有率第一，取得了众多汽车制造厂商优秀供应商等荣誉，获得比亚迪、蔚来汽车、特斯拉等知名汽车制造厂商的多个车系的新项目定点信。<br>3、2023年9月27日晚公告，公司预计2023年前三季度，营业收入约15.6亿元，同比增长约30.22%，扣非净利润约1.06亿元，较上年同期增长96.97%；年Q3营业收入约6亿元，较Q2环比增长17.42%，同比增长25%，扣非净利润约3,500.00万元，同比增长60.09%。<br>4、2023年6月12日晚消息，沙特与华人运通签署一项价值约合56亿美元协议，公司已进入华人运通新项目定点。公司拥有声学产品、系统方案及相关算法的研发设计能力，产品主要涵盖车载扬声器系统、车载功放及AVAS，能够为客户提供全面的产品解决方案。"}
[{20230704}]{"code":"688533","name":"上声电子","date":"2023-07-04","bkmc":"新能源汽车","reason":"2023年7月1日，多家车企披露6月交付数据高增。2023年7月2日特斯拉Q2交付量创新高。2023年7月3日下午我国第2000万辆新能源汽车在广州下线，工信部表示将优化支持新能源汽车购买使用政策。","lb":"","expound":"车载扬声器+整车音响+汽车电子<br>1、2023年6月12日晚消息，沙特与华人运通签署一项价值约合56亿美元协议，公司已进入华人运通新项目定点。公司拥有声学产品、系统方案及相关算法的研发设计能力，产品主要涵盖车载扬声器系统、车载功放及AVAS，能够为客户提供全面的产品解决方案。公司车载扬声器在全球乘用车及轻型商用车市场的占有率为12.92%，国内市场占有率第一。<br>2、公司主要客户为大众集团、福特集团、博士视听等知名汽车制造厂商及电声品牌商，取得了众多汽车制造厂商优秀供应商等荣誉。<br>3、公司已获得比亚迪、蔚来汽车、特斯拉等知名汽车制造厂商的多个车系的新项目定点信。"}
[{20230613}]{"code":"688533","name":"上声电子","date":"2023-06-13","bkmc":"汽车产业链","reason":"2023年6月8日盘后，商务部通知指出，将组织开展汽车促消费活动，活动时间为2023年6月至12月。","lb":"","expound":"车载扬声器+整车音响+汽车电子<br>1、2023年6月12日晚消息，沙特与华人运通签署一项价值约合56亿美元协议，公司已进入华人运通新项目定点。公司拥有声学产品、系统方案及相关算法的研发设计能力，产品主要涵盖车载扬声器系统、车载功放及AVAS，能够为客户提供全面的产品解决方案。公司车载扬声器在全球乘用车及轻型商用车市场的占有率为12.92%，国内市场占有率第一。<br>2、公司主要客户为大众集团、福特集团、博士视听等知名汽车制造厂商及电声品牌商，取得了众多汽车制造厂商优秀供应商等荣誉。<br>3、公司已获得比亚迪、蔚来汽车、特斯拉等知名汽车制造厂商的多个车系的新项目定点信。"}
[{20221215}]{"code":"688533","name":"上声电子","date":"2022-12-15","bkmc":"汽车产业链","reason":"12月14日新华社报道，中共中央、国务院印发《扩大内需战略规划纲要(2022-2035年)》，《纲要》指出，释放出行消费潜力。优化城市交通网络布局，大力发展智慧交通。推动汽车消费由购买管理向使用管理转变。","lb":"","expound":"车载扬声器+整车音响+汽车电子<br>1、公司拥有声学产品、系统方案及相关算法的研发设计能力，产品主要涵盖车载扬声器系统、车载功放及AVAS，能够为客户提供全面的产品解决方案。公司车载扬声器在全球乘用车及轻型商用车市场的占有率为12.92%，国内市场占有率第一。<br>2、公司主要客户为大众集团、福特集团、博士视听等知名汽车制造厂商及电声品牌商，取得了众多汽车制造厂商优秀供应商等荣誉。<br>3、公司已获得比亚迪、蔚来汽车、特斯拉等知名汽车制造厂商的多个车系的新项目定点信。"}
[{20221102}]{"code":"688533","name":"上声电子","date":"2022-11-02","bkmc":"新能源车产业链","reason":"燃油车减半征收车辆购置税，新能源免征购置税延续至明年底，叠加多地出台的置换补贴，政策刺激力度大、延续性强、覆盖面广","lb":"","expound":"三季报增长+车载扬声器+整车音响+汽车电子<br>1、22年10月28日公告，公司前三季度营业收入 12.01亿元，同比增长30.96%。净利润5859.79万元，同比增长32.21%。<br>2、公司拥有声学产品、系统方案及相关算法的研发设计能力，产品主要涵盖车载扬声器系统、车载功放及AVAS，能够为客户提供全面的产品解决方案。公司车载扬声器在全球乘用车及轻型商用车市场的占有率为12.92%，国内市场占有率第一。<br>3、公司主要客户为大众集团、福特集团、博士视听等知名汽车制造厂商及电声品牌商，取得了众多汽车制造厂商优秀供应商等荣誉。<br>4、公司已获得比亚迪、蔚来汽车、特斯拉等知名汽车制造厂商的多个车系的新项目定点信。"}
[{20221012}]{"code":"688533","name":"上声电子","date":"2022-10-12","bkmc":"汽车产业链","reason":"广州发改委近期表示，2022年下半年个人消费者购买的纯电车型若符合“产品制造具备前后一体式铸铝车身成型工艺”及其他相关指标的可获购车补贴。","lb":"","expound":"车载扬声器+整车音响+汽车电子<br>1、公司2021年车载扬声器产品销量为6000万只增长4%，车载功放销量11.36万只增长100%；AVAS产品销量56.64万只增长134%；价格角度，平均每只价格约19.0元，单价相比上年同比增长约10.4%。公司已经开始充分受益于车载扬声器行业量价齐升趋势。<br>2、公司拥有声学产品、系统方案及相关算法的研发设计能力，产品主要涵盖车载扬声器系统、车载功放及AVAS，能够为客户提供全面的产品解决方案。公司车载扬声器在全球乘用车及轻型商用车市场的占有率为12.92%，国内市场占有率第一。<br>3、公司主要客户为大众集团、福特集团、博士视听等知名汽车制造厂商及电声品牌商，取得了众多汽车制造厂商优秀供应商等荣誉。<br>4、公司已获得比亚迪、蔚来汽车、特斯拉等知名汽车制造厂商的多个车系的新项目定点信。（详细解析请查阅21年4月19日异动解析）"}
[{20220729}]{"code":"688533","name":"上声电子","date":"2022-07-29","bkmc":"汽车产业链","reason":"","lb":"","expound":"车载扬声器+整车音响+汽车电子<br>1、公司2021年车载扬声器产品销量为6000万只增长4%，车载功放销量11.36万只增长100%；AVAS产品销量56.64万只增长134%；价格角度，平均每只价格约19.0元，单价相比上年同比增长约10.4%。公司已经开始充分受益于车载扬声器行业量价齐升趋势。<br>2、公司拥有声学产品、系统方案及相关算法的研发设计能力，产品主要涵盖车载扬声器系统、车载功放及AVAS，能够为客户提供全面的产品解决方案。公司车载扬声器在全球乘用车及轻型商用车市场的占有率为12.92%，国内市场占有率第一。<br>3、公司主要客户为大众集团、福特集团、博士视听等知名汽车制造厂商及电声品牌商，取得了众多汽车制造厂商优秀供应商等荣誉。<br>4、公司已获得比亚迪、蔚来汽车、特斯拉等知名汽车制造厂商的多个车系的新项目定点信。（详细解析请查阅21年4月19日异动解析）"}
[{20220627}]{"code":"688533","name":"上声电子","date":"2022-06-27","bkmc":"半导体","reason":"","lb":"","expound":"车载扬声器+整车音响<br>1、 公司自主研发国内电声行业首款低功耗、高性能数字化扬声器系统的SoC芯片，该芯片具有低功耗、低失真、高响度级、高清晰度和高集成度的技术优势。<br>2、公司拥有声学产品、系统方案及相关算法的研发设计能力，产品主要涵盖车载扬声器系统、车载功放及AVAS，能够为客户提供全面的产品解决方案。公司车载扬声器在全球乘用车及轻型商用车市场的占有率为12.92%，国内市场占有率第一。<br>3、公司主要客户为大众集团、福特集团、博士视听等知名汽车制造厂商及电声品牌商，取得了众多汽车制造厂商优秀供应商等荣誉。<br>4、公司已获得比亚迪、蔚来汽车、特斯拉等知名汽车制造厂商的多个车系的新项目定点信。（详细解析请查阅21年4月19日异动解析）"}
[{20220623}]{"code":"688533","name":"上声电子","date":"2022-06-23","bkmc":"汽车产业链","reason":"","lb":"","expound":"车载扬声器+整车音响<br>1、公司拥有声学产品、系统方案及相关算法的研发设计能力，产品主要涵盖车载扬声器系统、车载功放及AVAS，能够为客户提供全面的产品解决方案。公司车载扬声器在全球乘用车及轻型商用车市场的占有率为12.92%，国内市场占有率第一。<br>2、公司主要客户为大众集团、福特集团、博士视听等知名汽车制造厂商及电声品牌商，取得了众多汽车制造厂商优秀供应商等荣誉。<br>3、21年底公司已获得比亚迪、蔚来汽车、特斯拉等知名汽车制造厂商的多个车系的新项目定点信。（详细解析请查阅21年4月19日异动解析）"}
[{20220622}]{"code":"688533","name":"上声电子","date":"2022-06-22","bkmc":"汽车产业链","reason":"中国5月份新能源汽车销量44.7万辆，同比增长105.2%，拉动汽车产业链需求","lb":"","expound":"车载扬声器+整车音响<br>1、22年6月20日公司在调研中指出，公司主要客户为大众集团、福特集团等知名汽车制造厂商及电声品牌商。截止21年底，上声电子在全球车载扬声器产品的市占率达12.92%。<br>2、公司拥有声学产品、系统方案及相关算法的研发设计能力，产品主要涵盖车载扬声器系统、车载功放及AVAS，能够为客户提供全面的产品解决方案。带控制模模块的AVAS单价在100元以上。公司产品出口地区主要为美国、德国、捷克等国家<br>3、21年底公司已获得比亚迪、蔚来汽车、特斯拉等知名汽车制造厂商的多个车系的新项目定点信。（详细解析请查阅21年4月19日异动解析）"}
