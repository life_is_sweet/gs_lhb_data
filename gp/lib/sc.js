    var result = [];

    function getData() {
        getSCNN();
        getZHQD();
        getBSZJ();
        getZSQK();
        getQQZS();
        getZDJS();
        getSCQX();
        getZB();
        return result;
    }

 //综合强度
    function getZHQD() {
        $.ajax({
            type: "get",
            url: "https://apphq.longhuvip.com/w1/api/index.php?a=DiskReview&apiv=w31&c=HomeDingPan",
            dataType: "json",
            async : false,
            cache:false,
            timeout:3000,
            error: function() {
                //alert("失败，请稍后再试！");
            },
            success: function(data) {
                result["ZHQD"] = data.info.strong;
            }
        });
    }
    //市场量能
    function getSCNN() {
        $.ajax({
            type: "get",
            url: "https://apphq.longhuvip.com/w1/api/index.php?a=MarketCapacity&apiv=w31&Type=0&c=HomeDingPan&PhoneOSNew=1",
            dataType: "json",
            async : false,
            cache:false,
            timeout:3000,
            error: function() {
                //alert("失败，请稍后再试！");
            },
            success: function(data) {
                var scnn = [];
                scnn['last'] = data.info.last;
                scnn['s_zrcs'] = data.info.s_zrcs;
                scnn['s_zrtj'] = data.info.s_zrtj;
                scnn['space'] = data.info.last-data.info.s_zrcs;    
                result['SCNN'] = scnn;
            }
        });
    }
    //北上资金
    function getBSZJ() {
        $.ajax({
            type: "get",
            url: "https://apphq.longhuvip.com/w1/api/index.php?a=NorthboundFundsB&apiv=w31&c=HomeDingPan&PhoneOSNew=1",
            dataType: "json",
            async : false,
            cache:false,
            timeout:3000,
            error: function() {
                //alert("失败，请稍后再试！");
            },
            success: function(data) {
                var bszj = [];
                var index = data.info.trend.length-1;
                bszj["time"] = data.info.trend[index][0];
                bszj["hgt"] = data.info.trend[index][1];
                bszj["sgt"] = data.info.trend[index][2];
                bszj["space"] = data.info.trend[index][3];   
                result['BSZJ'] = bszj;
            }
        });
    }
    //指数情况
    function getZSQK() {
        $.ajax({
            type: "get",
            url: "https://apphq.longhuvip.com/w1/api/index.php?a=RefreshStockList&apiv=w31&c=UserSelectStock&StockIDList=SH000001%2CSZ399001%2CSZ399006%2CSH000688%2CSH000016%2CSH000300&",
            dataType: "json",
            async : false,
            cache:false,
            timeout:3000,
            error: function() {
                //alert("失败，请稍后再试！");
            },
            success: function(data) {
                var zsqk = [];
                zsqk['szzs'] = {name:"上证指数",last_px:data.StockList[0]["last_px"],increase_rate:data.StockList[0]["last_px"],increase_amount:data.StockList[0]["increase_amount"]};
                zsqk['cybz'] = {name:"创业板指数",last_px:data.StockList[1]["last_px"],increase_rate:data.StockList[1]["last_px"],increase_amount:data.StockList[1]["increase_amount"]};
                zsqk['szzs50'] = {name:"上证50",last_px:data.StockList[5]["last_px"],increase_rate:data.StockList[5]["last_px"],increase_amount:data.StockList[5]["increase_amount"]};
                result['ZSQK'] = zsqk;
            }
        });
    }



   //全球指数
    function getQQZS() {
        $.ajax({
            type: "get",
            url: "https://apphq.longhuvip.com/w1/api/index.php?a=GlobalCommon&apiv=w31&c=GlobalIndex&PhoneOSNew=1&View=1%2C2%2C3%2C4%2C5%2C6&",
            dataType: "json",
            async : false,
            cache:false,
            timeout:3000,
            error: function() {
                //alert("失败，请稍后再试！");
            },
            success: function(data) {
                var qqzs = [];                
                qqzs['dqs'] = {name:"道琼斯",last_px:data.CYWWZS[0]["last_px"],increase_rate:data.CYWWZS[0]["increase_rate"],increase_amount:data.CYWWZS[0]["increase_amount"]};
                qqzs['nsdk'] = {name:"纳斯达克",last_px:data.CYWWZS[1]["last_px"],increase_rate:data.CYWWZS[1]["increase_rate"],increase_amount:data.CYWWZS[1]["increase_amount"]};
                qqzs['bp500'] = {name:"标普500",last_px:data.CYWWZS[2]["last_px"],increase_rate:data.CYWWZS[2]["increase_rate"],increase_amount:data.CYWWZS[2]["increase_amount"]};
                qqzs['rj225'] = {name:"日经225",last_px:data.CYWWZS[4]["last_px"],increase_rate:data.CYWWZS[4]["increase_rate"],increase_amount:data.CYWWZS[4]["increase_amount"]};
                qqzs['hszs'] = {name:"恒生指数",last_px:data.CYWWZS[3]["last_px"],increase_rate:data.CYWWZS[3]["increase_rate"],increase_amount:data.CYWWZS[3]["increase_amount"]};
                
      
                qqzs['qhzs_dqs'] = {name:"道琼斯期指",last_px:data.RMGZ[0]["last_px"],increase_rate:data.RMGZ[0]["increase_rate"],increase_amount:data.RMGZ[0]["increase_amount"]};
                qqzs['qhzs_a50'] = {name:"A50期指",last_px:data.RMGZ[1]["last_px"],increase_rate:data.RMGZ[1]["increase_rate"],increase_amount:data.RMGZ[1]["increase_amount"]};
                qqzs['qhzs_rz225'] = {name:"日经期指",last_px:data.RMGZ[2]["last_px"],increase_rate:data.RMGZ[2]["increase_rate"],increase_amount:data.RMGZ[2]["increase_amount"]};


                qqzs['qhzs_nyj'] = {name:"纽约金",last_px:data.QQSP[0]["last_px"],increase_rate:data.QQSP[0]["increase_rate"],increase_amount:data.QQSP[0]["increase_amount"]};
                qqzs['qhzs_nyt'] = {name:"纽约铜",last_px:data.QQSP[1]["last_px"],increase_rate:data.QQSP[1]["increase_rate"],increase_amount:data.QQSP[1]["increase_amount"]};
                qqzs['qhzs_trq'] = {name:"天然气",last_px:data.QQSP[2]["last_px"],increase_rate:data.QQSP[2]["increase_rate"],increase_amount:data.QQSP[2]["increase_amount"]};
                qqzs['qhzs_bltyy'] = {name:"布伦特原油",last_px:data.QQSP[4]["last_px"],increase_rate:data.QQSP[4]["increase_rate"],increase_amount:data.QQSP[5]["increase_amount"]};
                qqzs['qhzs_mynexyy'] = {name:"MYMEX原油",last_px:data.QQSP[5]["last_px"],increase_rate:data.QQSP[5]["increase_rate"],increase_amount:data.QQSP[5]["increase_amount"]};
                
                qqzs['qhzs_oymy'] = {name:"欧元兑美元",last_px:data.HLZS[0]["last_px"],increase_rate:data.HLZS[0]["increase_rate"],increase_amount:data.HLZS[0]["increase_amount"]};
                qqzs['qhzs_myrmb'] = {name:"美元人民币",last_px:data.HLZS[1]["last_px"],increase_rate:data.HLZS[1]["increase_rate"],increase_amount:data.HLZS[1]["increase_amount"]};
                qqzs['qhzs_myzs'] = {name:"美元指数",last_px:data.HLZS[2]["last_px"],increase_rate:data.HLZS[2]["increase_rate"],increase_amount:data.HLZS[2]["increase_amount"]};

                result['QQZS'] = qqzs;
            }
        });
    }

    //涨跌家数
    function getZDJS() {
        $.ajax({
            type: "get",
            url: "https://apphq.longhuvip.com/w1/api/index.php?a=ZhangFuDetail&apiv=w31&c=HomeDingPan&PhoneOSNew=1",
            dataType: "json",
            async : false,
            cache:false,
            timeout:3000,
            error: function() {
                //alert("失败，请稍后再试！");
            },
            success: function(data) {
                var zdjs = [];
                zdjs['dt'] = data.info.DT;
                zdjs['zt'] = data.info.ZT;
                zdjs['sjdt'] = data.info.SJDT;
                zdjs['sjzt'] = data.info.SJZT;
                zdjs['szjs'] =data.info.SZJS;
                zdjs['xdjs'] =data.info.XDJS;
                result['ZDJS'] = zdjs;
            }
        });

    }

    //破板
        function getZB() {
        $.ajax({
            type: "get",
            url: "https://apphq.longhuvip.com/w1/api/index.php?Order=1&a=DaBanList&st=999&c=HomeDingPan&PhoneOSNew=1&Index=0&Is_st=1&PidType=2&apiv=w31&Type=6&FilterMotherboard=0&Filter=0&FilterTIB=0&FilterGem=0&",
            dataType: "json",
            async : true,
            cache:false,
            timeout:3000,
            error: function() {
                //alert("失败，请稍后再试！");
            },
            success: function(data) {
                result['tZhaBan'] = data.list.length;
            }
        });
    }



    //市场情绪
    function  getSCQX(){
        $.ajax({
            type: "get",
            url: "https://apphq.longhuvip.com/w1/api/index.php?c=Index&a=GetInfo&View=2%2C3%2C4%2C5%2C7%2C8%2C9%2C10",
            dataType: "json",
            async : false,
            cache:false,
            timeout:3000,
            error: function() {
                //alert("失败，请稍后再试！");
            },
            success: function(data) {
                var scqx = [];
                scqx['SZJS'] = data.DaBanList.SZJS;
                scqx['XDJS'] = data.DaBanList.XDJS;
                scqx['lDieTing'] = data.DaBanList.lDieTing; // 昨日跌停
                scqx['lFengBan'] = data.DaBanList.lFengBan; // 昨日封板率
                scqx['lZhangTing'] = data.DaBanList.lZhangTing; // 昨日涨停
                scqx['tDieTing'] = data.DaBanList.tDieTing; // 跌停
                scqx['tFengBan'] = data.DaBanList.tFengBan; //  封板率                                      
                scqx['tZhangTing'] = data.DaBanList.tZhangTing; // 涨停
                scqx['yestRase'] = data.DaBanList.yestRase; // 破坏率
                result['SCQX'] = scqx;
            }
        });
    }

    //全球指数
    function viewQQZS(id,arr) {
        var name = arr.name;
        var last_px = arr.last_px;
        var increase_rate = arr.increase_rate;
        var increase_amount = arr.increase_amount;
        var mark = id.slice(1);

        $(id).html(" <p>"+name+"<br />"+last_px+"<br /><span id=\"span_"+mark+"\"> "+increase_amount+" "+increase_rate+"</span></p>");
        //console.log(" <p>"+name+"<br />"+last_px+"<br /><span id=\"span_"+mark+"\"> "+increase_amount+" "+increase_rate+"</span></p>");
        if (increase_amount <= 0) {
            $("#span_"+mark).css("color","#4AD4A7");
        }else {
            $("#span_"+mark).css("color","red");
        }
    }

    //市场量能
    function viewSCNN(data) {
        var last  = data.last*10000;
        var s_zrcs = data.s_zrcs*10000;
        var space = data.space*10000;
        //$("#scnn").html("今:"+onMoneyFormat(last) +"<br /> 昨:"+onMoneyFormat(s_zrcs)+" <span id=\"scnn_space\">"+onMoneyFormat(space)+"</span></p>");
        $("#scnn").html("<p>市场量能<br />今:"+onMoneyFormat(last) +"<br /> "+" <span id=\"scnn_space\">"+onMoneyFormat(space)+"</span></p>");
        if (space <= 0) {
            $("#scnn_space").css("color","#4AD4A7");
        }else {
            $("#scnn_space").css("color","red");
        }
    }
    //北上资金
    function viewBSZJ(data) {
        $("#bszj").html("<p>北上资金<br />"+onMoneyFormat(data.hgt) +onMoneyFormat(data.sgt)+"<br /> <span id=\"bszj_space\">"+onMoneyFormat(data.space)+"</span></p>");
        if (data.space <= 0) {
            $("#bszj_space").css("color","#4AD4A7");
        }else {
            $("#bszj_space").css("color","red");
        }
    }
    //涨跌家数
    function viewZDJS(data) {
        $("#zdjs").html("涨停(实): "+data.sjzt +"<br /> 跌停(实): "+data.sjdt+"</p>");
    }

    //综合情况
    function viewZHQD(data) {  
        $("#zhqd").html("<br>综合分数：<strong>"+data+"</strong>");
        if (data >= 75) {
            $("#zhqd").css("color","yellow");
        } else if (data>=50) {
            $("#zhqd").css("color","red");
        } else if (data>=25) {
            $("#zhqd").css("color","orange");
        } else {
            $("#zhqd").css("color","#4AD4A7");
        }
    }

    function viewSCQX(data) {
         $("#scqx").html( "封板率：<strong><span id=\"tFengBan\">"+
Math.round(data.tFengBan)+"</span></strong>/"+ Math.round(data.lFengBan)+"<br />涨停：<strong><span id=\"tZhangTing\">"+data.tZhangTing+"</span></strong>/"+ data.lZhangTing+"<br />跌停：<strong><span id=\"tDieTing\">"+data.tDieTing+"</strong>/"+ data.lDieTing);
        if (data.tZhangTing > data.lZhangTing) {
            $("#tZhangTing").css("color","red");
        }else{
             $("#tZhangTing").css("color","green");
        }
        if (data.tDieTing > data.lDieTing) {
            $("#tDieTing").css("color","yellow");
        }else{
             $("#tDieTing").css("color","green");
        }
        if (data.tFengBan > data.lFengBan) {
            $("#tFengBan").css("color","red");
        }else{
             $("#tFengBan").css("color","green");
        }
    }

    function viewSCZD(data) {
        var pbl = Math.round(result["tZhaBan"]/(result['SCQX']["tZhangTing"]+result["tZhaBan"])*100)+"%";
        $("#sczd").html("涨跌家数<br /><strong><span style=\"color:red;\">"+data.SZJS+"</span></strong> / <span style=\"color:green;\">"+ data.XDJS+"</span><br />破板率：<strong>"+pbl+"</strong>");

    }

    //增加超链接
    function AddLink() {
        $("#hszs").wrap('<a href="http://www.treeid/code_HSI" />');
        $("#qhzs_a50").wrap('<a href="http://www.treeid/code_CNY0" />');
    }

