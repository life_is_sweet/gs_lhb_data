function makeFriendly(num) {
    if (num >= 100000000) {
        num = Math.round(num / 10000000) / 10 + '亿'
    } else if (num >= 10000) {
        num = Math.round(num / 1000) / 10 + '万'
    }
    return num;
}

function onMoneyFormat(money) {
  if (money < 0) {
    return "-"+ makeFriendly(Math.abs(money));
  } else {
    return makeFriendly(money);
  }
}