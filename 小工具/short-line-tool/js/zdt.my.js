new Vue({
	el:'#datatable',
	data:{
		list:[],
		originList:[],
		activeIndex:-1,
		hoverIndex:-1,
		sortColIndex:-1,
		sortDirection:''
	},
	created:function(){},
	mounted:function(){
			this.$forceUpdate();
			let refreshIntervalId;
			const refreshData=()=>{
				let self=this;
				self.originList=[];
				self.list=[];
				jQuery.getJSON(urlz,{pool_name:ltype},function(c){
					if(c && c.data && c.data.length>0){
						(c.data || []).forEach(xx=>{
							self.originList.push(xx);
						})
					}
					if(dxb){
						jQuery.getJSON(urlz,{pool_name:'limit_up_broken'},function(a){
							if(a && a.data && a.data.length>0){
								(a.data || []).forEach(xx=>{
									self.originList.push(xx);
								})
							}
						})
					}
					if(self.originList.length>0)
						self.loadKaiPanJia();					
					//self.originList.sort(function(a,b){
					//	return b.first_limit_up-a.first_limit_up
					//})
				});
				let rebind_reason = function(){
					self.$nextTick(()=>{					
						jQuery('.text-truncate').each(function(e,i){
							jQuery(this).html(jQuery(this).attr('rel'))
						})					
					})
				}
				rebind_reason();
				jQuery('#datatable th').click(function(){
					if(self.originList.length<1)
						return;
					self.sortColIndex = jQuery(this).index();
					let key = jQuery(this).attr('data-sort-key');
					if(jQuery(this).hasClass('sorting')){
						jQuery(this).removeClass('sorting');
						jQuery(this).addClass('sorting_desc');
						jQuery(this).addClass('active');
						self.sortDirection = 'desc';
						let sbs = jQuery(this).siblings();
						sbs.removeClass('active');	
						sbs.removeClass('sorting_desc');							
						sbs.removeClass('sorting_asc');	
						for(let cc = 0;cc < sbs.length; cc++) {
							if(jQuery(sbs[cc]).attr('data-sort-key')){
								jQuery(sbs[cc]).addClass('sorting');
							}
						}
						 self.list.sort(function(a,b){
							if(key=='symbol'){
								return b[key].replace(".SH",'').replace(".SZ",'').replace(".SS",'').localeCompare(a[key].replace(".SH",'').replace(".SZ",'').replace(".SS",''));
							}
							if(key=='fengdan'){
								let afd = a.isBroken?0:(a.non_restricted_capital || 0)*(a.buy_lock_volume_ratio ||0);
								let bfd = b.isBroken?0:(b.non_restricted_capital || 0)*(b.buy_lock_volume_ratio ||0);
								return bfd-afd;
							}								
							if(key == 'fengdan_die')
								return (b.non_restricted_capital || 0)*(b.sell_lock_volume_ratio ||0) - (a.non_restricted_capital || 0)*(a.sell_lock_volume_ratio ||0);								
							if(key == 'zonge')//总额
								return (b.turnover_ratio || 0)*(b.non_restricted_capital || 0) - (a.turnover_ratio || 0)*(a.non_restricted_capital || 0);
							if(key == 'boards_days_boards')
								return b.m_days_n_boards_days-a.m_days_n_boards_days
							if(key == 'fk_reason')
								return self.moreInfo(b.surge_reason).localeCompare(self.moreInfo(a.surge_reason))
							if(typeof(a[key])=='string' && isNaN(a[key]))
								return b[key].localeCompare(a[key])								
							else
								return b[key]-a[key];
						})							
					}else if(jQuery(this).hasClass('sorting_desc')){
						jQuery(this).removeClass('sorting_desc');
						jQuery(this).addClass('sorting_asc');
						jQuery(this).addClass('active');
						self.sortDirection = 'asc';
						let sbs = jQuery(this).siblings();
						sbs.removeClass('active');
						sbs.removeClass('sorting_desc');
						sbs.removeClass('sorting_asc');	
						for(let cc = 0;cc < sbs.length; cc++) {
							if(jQuery(sbs[cc]).attr('data-sort-key')){
								jQuery(sbs[cc]).addClass('sorting');
							}
						}
						self.list.sort(function(a,b){
							if(key=='symbol'){
								return a[key].replace(".SH",'').replace(".SZ",'').replace(".SS",'').localeCompare(b[key].replace(".SH",'').replace(".SZ",'').replace(".SS",''))
							}
							if(key=='fengdan'){
								let afd = a.isBroken?0:(a.non_restricted_capital || 0)*(a.buy_lock_volume_ratio ||0);
								let bfd = b.isBroken?0:(b.non_restricted_capital || 0)*(b.buy_lock_volume_ratio ||0);
								return afd - bfd;
							}								
							if(key == 'fengdan_die')
								return (a.non_restricted_capital || 0)*(a.sell_lock_volume_ratio ||0) - (b.non_restricted_capital || 0)*(b.sell_lock_volume_ratio ||0);								
							if(key == 'zonge')//总额
								return (a.turnover_ratio || 0)*(a.non_restricted_capital || 0) - (b.turnover_ratio || 0)*(b.non_restricted_capital || 0);
							if(key == 'boards_days_boards')
								return a.m_days_n_boards_days-b.m_days_n_boards_days
							if(key == 'fk_reason')
								return self.moreInfo(a.surge_reason).localeCompare(self.moreInfo(b.surge_reason))
							if(typeof(a[key])=='string' && isNaN(a[key]))									
								return a[key].localeCompare(b[key])
							else
								return a[key]-b[key];
						})							
					}else if(jQuery(this).hasClass('sorting_asc')){
						jQuery(this).removeClass('sorting_asc');
						jQuery(this).addClass('sorting');
						jQuery(this).removeClass('active');
						self.sortDirection = '';
						self.sortColIndex = -1;
						self.list=[];
						(self.originList || []).forEach(xx=>{
							self.list.push(xx);
						});
					}
					rebind_reason();
				});
				
				if(dxb){
					if(self.sortColIndex>-1&&self.sortDirection!=''){
						console.log(self.sortColIndex,self.sortDirection);
						let iii=3;
						let sci = self.sortColIndex;
						while(iii>0){
							iii--;
							setTimeout(function(){
								var ev = document.createEvent('MouseEvents');
								ev.initEvent('click',true,true);
								jQuery('#datatable th')[sci].dispatchEvent(ev);
							},1);
						}
					}else{
						//初始化排序
						jQuery(jQuery('#datatable th')[5]).addClass('sorting_desc');
						jQuery(jQuery('#datatable th')[5]).addClass('active');
						let kk = jQuery(jQuery('#datatable th')[5]).attr('data-sort-key');
						self.list.sort(function(a,b){
							return b[kk]-a[kk];
						});
					}					
				}
			};
			const isWorkingHour=()=>{
				const now=new Date();
				const dayOfWeek=now.getDay();
				const hours=now.getHours();
				return dayOfWeek>=1&&dayOfWeek<=5&&hours>=9&&hours<15
			};
			if(isWorkingHour()){
				refreshData();
				let tt = 6000;
				if(dxb){
					tt = 15000
				}
				refreshIntervalId=setInterval(refreshData,tt);
			} else {
				refreshData()
			}
	},
	methods:{
		moreInfo(s){
			if(s && typeof(s.related_plates)!=='undefined'&&s.related_plates!==null){
				str='';
				for(var i=0;i<s.related_plates.length;i++){
					if(i>0)str+="+";
					str+=s.related_plates[i].plate_name
				}				
				str+=s.stock_reason;
				return str
			}
			else return false
		},
		loadKaiPanJia(){
			let self = this;
			let arr_codes = (self.originList || []).flatMap(ff=>{
				let cc = ff.symbol.split('.');
				return (cc[1].toLowerCase()+cc[0]).replace(/ss/gi,'sh');
			});
			let codes = arr_codes.join(',');
			let settings = {
			  "url": "http://sqt.gtimg.cn/q="+codes+"&offset=5,6,10,11,20,21",
			  "method": "GET",
			  "timeout": 0,
			};
			
			$.ajax(settings).done(function (response) {	
				if(response){
					let arr_rows = response.split('\n');
					if(arr_rows.length<1){
						return;
					}
					for(let tt=0;tt<arr_rows.length;tt++){
						if(!arr_rows[tt] || arr_rows[tt].length<1){
							continue;
						}
						let ff = arr_rows[tt].split('=');
						let req_code = ff[0].replace('v_','');
						let price =  ff[1].replace(/\"/gi,'').replace(/;/gi,'').split('~');
						let sa = (((price[1]-price[0])/price[0])*100);
						let ss = sa.toFixed(2)+'%';
						//$('td[data-id='+req_code+']').text(ss);
						
						for(let ii=0;ii<self.originList.length;ii++){
							if((self.originList[ii].symbol||'').match(/\d{6}/gi)[0]==req_code.match(/\d{6}/gi)[0]){								
								self.originList[ii].kaipanjia = sa;
								self.originList[ii].kaipanjia_percent = ss;
								if(price[4] ==0 && price[5]==0){//卖一价，卖一多少手，0标识涨停
									self.originList[ii].fengdan_e_du = (price[2]*price[3])*100;
								}else if(price[2] ==0 && price[3]==0){//买一价，买一多少手，0标识跌停
									self.originList[ii].fengdan_e_du = (price[4]*price[5])*100;
								}else{
									self.originList[ii].fengdan_e_du = 0;
								}
								break;
							}	
						}
					}
				}
				
			}).done(function(){
					(self.originList || []).forEach(xx=>{
						self.list.push(xx);
					});
			});
		},
		handleMouseOver(index){
			this.hoverIndex=index
		},
		handleMouseLeave(index){
			this.hoverIndex=-1
		},
		handleClick(index,event){
			this.activeIndex=index;
			//window.location.href='http://www.treeid/code_'+$('tbody tr:eq('+index+')').attr('dm')
		},
	}
});